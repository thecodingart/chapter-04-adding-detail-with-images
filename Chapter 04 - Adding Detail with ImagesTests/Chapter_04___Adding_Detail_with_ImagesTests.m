//
//  Chapter_04___Adding_Detail_with_ImagesTests.m
//  Chapter 04 - Adding Detail with ImagesTests
//
//  Created by Brandon Levasseur on 2/15/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>

@interface Chapter_04___Adding_Detail_with_ImagesTests : XCTestCase

@end

@implementation Chapter_04___Adding_Detail_with_ImagesTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
