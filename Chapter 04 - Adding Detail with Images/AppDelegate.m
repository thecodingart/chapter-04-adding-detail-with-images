//
//  AppDelegate.m
//  Chapter 04 - Adding Detail with Images
//
//  Created by Brandon Levasseur on 2/15/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import "AppDelegate.h"
@import SceneKit;

@interface AppDelegate ()
@property (weak) IBOutlet SCNView *sceneView;
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)awakeFromNib
{
    SCNScene *scene = self.sceneView.scene;
    SCNNode *shipNode = [scene.rootNode childNodeWithName:@"Spaceship" recursively:YES];
    SCNGeometry *spaceship = shipNode.geometry;
    spaceship.firstMaterial.diffuse.contents = [NSImage imageNamed:@"diffuse.png"];
    spaceship.firstMaterial.locksAmbientWithDiffuse = YES;
    
    spaceship.firstMaterial.specular.contents = [NSImage imageNamed:@"multiply.png"];
}

@end
