//
//  main.m
//  Chapter 04 - Adding Detail with Images
//
//  Created by Brandon Levasseur on 2/15/15.
//  Copyright (c) 2015 Brandon Levasseur. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
